# jhugo24
This project is intended to be a framework for hosting my personal projects, including my art, comics, music, and worldbuidling documents.

My priorities are:
- Secure
- Lightweight
- Ease of use at entry level

## Requirements
This tool currently uses [Hugo](https://github.com/gohugoio/hugo), and work is underway to integrate [DokuWiki](https://www.dokuwiki.org/dokuwiki).

## Installation

1. [Install Hugo](https://gohugo.io/installation/) on your server
2. Clone this repo to your server

## Contributing
Please direct merge requests to me, Justice Watch. I will be very picky though sorry

## Authors and acknowledgment
Thank you Sarrow Seviper for helping me start this project, even though I undid all your commits. And thank you Wizpig64 for holding my hand through everything.

## License
I really don't care what you use this for, go hog wild. Maybe I should update this section later.